FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY target/docker-maven-plugin-*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]